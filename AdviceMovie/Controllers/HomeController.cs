﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace AdviceMovie.Controllers
{
    public class HomeController : Controller
    {
        private List<Movie> movies;
        private SqlConnection connection;
        public ActionResult Index()
        {
            movies = new List<Movie>();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        [HttpPost]
        public ActionResult FindButton(string tags)
        {
            ViewBag.movies = new List<Movie>();
            connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
            connection.Open();
            movies = new List<Movie>();
            var tempTags = tags.Split(',');
            for (int i = 0; i < tempTags.Length; i++)
            {
                if (i == 0)
                {
                    SqlCommand sqlCommand = new SqlCommand($"SELECT MovieTitel, ScoreImdb, MovieImdbLink, Genres, DirectorName,ActoreOneName, ActoreTwoName, ActoreThreeNames, PlotKeywords FROM dbo.MOVIES WHERE DirectorName='{tempTags[i]}'", connection);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    searchWhere(sqlDataReader.HasRows, sqlDataReader);
                    sqlDataReader.Close();
                    SqlCommand sqlCommand1 = new SqlCommand($"SELECT MovieTitel, ScoreImdb, MovieImdbLink, Genres, DirectorName,ActoreOneName, ActoreTwoName, ActoreThreeNames, PlotKeywords FROM dbo.MOVIES WHERE ActoreOneName LIKE'%{tempTags[i]}%'", connection);
                    SqlDataReader sqlDataReader1 = sqlCommand1.ExecuteReader();
                    searchWhere(sqlDataReader1.HasRows, sqlDataReader1);
                    sqlDataReader1.Close();
                    SqlCommand sqlCommand2 = new SqlCommand($"SELECT MovieTitel, ScoreImdb, MovieImdbLink, Genres, DirectorName,ActoreOneName, ActoreTwoName, ActoreThreeNames, PlotKeywords FROM dbo.MOVIES WHERE ActoreTwoName LIKE'%{tempTags[i]}%'", connection);
                    SqlDataReader sqlDataReader2 = sqlCommand2.ExecuteReader();
                    searchWhere(sqlDataReader2.HasRows, sqlDataReader2);
                    sqlDataReader2.Close();
                    SqlCommand sqlCommand3 = new SqlCommand($"SELECT MovieTitel, ScoreImdb, MovieImdbLink, Genres, DirectorName,ActoreOneName, ActoreTwoName, ActoreThreeNames, PlotKeywords FROM dbo.MOVIES WHERE ActoreThreeNames LIKE'%{tempTags[i]}%'", connection);
                    SqlDataReader sqlDataReader3 = sqlCommand3.ExecuteReader();
                    searchWhere(sqlDataReader3.HasRows, sqlDataReader3);
                    sqlDataReader3.Close();
                    SqlCommand sqlCommand4 = new SqlCommand($"SELECT MovieTitel, ScoreImdb, MovieImdbLink, Genres, DirectorName,ActoreOneName, ActoreTwoName, ActoreThreeNames, PlotKeywords FROM dbo.MOVIES WHERE PlotKeywords LIKE'%{tempTags[i]}%'", connection);
                    SqlDataReader sqlDataReader4 = sqlCommand4.ExecuteReader();
                    searchWhere(sqlDataReader4.HasRows, sqlDataReader4);
                    sqlDataReader4.Close();
                    SqlCommand sqlCommand5 = new SqlCommand($"SELECT MovieTitel, ScoreImdb, MovieImdbLink, Genres, DirectorName,ActoreOneName, ActoreTwoName, ActoreThreeNames, PlotKeywords FROM dbo.MOVIES WHERE Genres LIKE'%{tempTags[i]}%'", connection);
                    SqlDataReader sqlDataReader5 = sqlCommand5.ExecuteReader();
                    searchWhere(sqlDataReader5.HasRows, sqlDataReader5);
                    sqlDataReader5.Close();
                    SqlCommand sqlCommand6 = new SqlCommand($"SELECT MovieTitel, ScoreImdb, MovieImdbLink FROM dbo.MOVIES WHERE MovieTitel='{tempTags[i]}'", connection);
                    SqlDataReader sqlDataReader6 = sqlCommand6.ExecuteReader();
                    searchWhere(sqlDataReader6.HasRows, sqlDataReader6);
                    sqlDataReader6.Close();
                    movies = movies.OrderByDescending(x => x.ScoreImdb).ToList();
                    ViewBag.movies = movies;
                }
                else
                {
                    List<Movie> tempDirector = new List<Movie>() { new Movie() {DirectorName = tempTags[i]} };
                    ViewBag.movies = movies.Where(m => tempDirector.Any(mm => m.DirectorName == mm.DirectorName)).ToList();
                }
            }
            return View("Index");
        }

        private void searchWhere(bool HasRow, SqlDataReader sqlDataReader)
        {
            if (HasRow)
            {
                while (sqlDataReader.Read())
                {
                    Movie movie = new Movie();
                    movie.MovieTitel = sqlDataReader["MovieTitel"].ToString();
                    movie.ScoreImdb = sqlDataReader["ScoreImdb"].ToString();
                    movie.MovieImdbLink = sqlDataReader["MovieImdbLink"].ToString();
                    movie.Genres = sqlDataReader["Genres"].ToString();
                    movie.DirectorName = sqlDataReader["DirectorName"].ToString();
                    movie.ActoreOneName = sqlDataReader["ActoreOneName"].ToString();
                    movie.ActoreTwoName = sqlDataReader["ActoreTwoName"].ToString();
                    movie.ActoreThreeNames = sqlDataReader["ActoreThreeNames"].ToString();
                    movie.PlotKeywords = sqlDataReader["PlotKeywords"].ToString();
                    movies.Add(movie);
                }

            }
        }


    }
}