﻿namespace AdviceMovie
{
    public class Movie
    {
        public int Id { get; set; }

        public string Color { get; set; }

        public string DirectorName { get; set; }

        public double NumberCriticForReviews { get; set; }

        public double Duration { get; set; }

        public double DirectorFacebookLikes { get; set; }

        public double ActoreThreeFacebookLikes { get; set; }

        public string ActoreThreeNames { get; set; }

        public double ActoreTwoFacebookLikes { get; set; }

        public string ActoreTwoName { get; set; }

        public double ActoreOneFacebookLikes { get; set; }

        public string ActoreOneName { get; set; }

        public double Gross { get; set; }

        public string Genres { get; set; }

        public string MovieTitel { get; set; }

        public double NumberVotedUsers { get; set; }

        public double CastTotalFacebookLikes { get; set; }

        public double FaceNumberInPoster { get; set; }

        public string PlotKeywords { get; set; }

        public string MovieImdbLink { get; set; }

        public double NumberUserforReviews { get; set; }

        public string Language { get; set; }

        public string Country { get; set; }

        public string ContentRating { get; set; }

        public double Budget { get; set; }

        public double TitleYear { get; set; }

        public string ScoreImdb { get; set; }

        public double MovieFacebookLikes { get; set; }
    }
}
